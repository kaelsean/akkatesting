﻿using Akka.TestKit.Xunit2;
using AkkaTesting.DeviceApp.Actors;
using AkkaTesting.DeviceApp.Messages.Device;
using AkkaTesting.DeviceApp.Messages.DeviceGroup;
using AkkaTesting.DeviceApp.Messages.Temperature;
using FluentAssertions;
using Xunit;

namespace AkkaTesting.Test
{
    public class DeviceManagerActorTests : TestKit
    {
        [Fact]
        public void DeviceManager_actor_must_be_able_to_register_a_device_actor()
        {
            var probe = CreateTestProbe();
            var managerActor = Sys.ActorOf(DeviceManagerActor.Props());
            
            managerActor.Tell(new RequestTrackDevice("group", "device1"), probe.Ref);
            probe.ExpectMsg<DeviceRegistered>();
            var deviceActor1 = probe.LastSender;
            
            managerActor.Tell(new RequestTrackDevice("group", "device2"), probe.Ref);
            probe.ExpectMsg<DeviceRegistered>();
            var deviceActor2 = probe.LastSender;
            
            deviceActor1.Should().NotBe(deviceActor2);
            
            deviceActor1.Tell(new RecordTemperature(1, 25.0), probe.Ref);
            probe.ExpectMsg<TemperatureRecorded>(x => x.RequestId == 1);

            deviceActor2.Tell(new RecordTemperature(2, 47.0), probe.Ref);
            probe.ExpectMsg<TemperatureRecorded>(x => x.RequestId == 2);
        }
        
        [Fact]
        public void DeviceManager_actor_must_return_same_actor_for_same_deviceId()
        {
            var probe = CreateTestProbe();
            var managerActor = Sys.ActorOf(DeviceManagerActor.Props());

            managerActor.Tell(new RequestTrackDevice("group", "device1"), probe.Ref);
            probe.ExpectMsg<DeviceRegistered>();
            var deviceActor1 = probe.LastSender;

            managerActor.Tell(new RequestTrackDevice("group", "device1"), probe.Ref);
            probe.ExpectMsg<DeviceRegistered>();
            var deviceActor2 = probe.LastSender;

            deviceActor1.Should().Be(deviceActor2);
        }
        
        [Fact]
        public void DeviceManager_actor_must_be_able_to_list_active_groups()
        {
            var probe = CreateTestProbe();
            var managerActor = Sys.ActorOf(DeviceManagerActor.Props());

            managerActor.Tell(new RequestTrackDevice("group1", "device1"), probe.Ref);
            probe.ExpectMsg<DeviceRegistered>();

            managerActor.Tell(new RequestTrackDevice("group2", "device2"), probe.Ref);
            probe.ExpectMsg<DeviceRegistered>();

            managerActor.Tell(new RequestDeviceGroupList(1), probe.Ref);
            probe.ExpectMsg<ReplyDeviceGroupList>(x => x.RequestId == 1 &&
                                                  x.Ids.Contains("group1") &&
                                                  x.Ids.Contains("group2"));
        }
        
/*
        [Fact]
        public void DeviceManager_actor_must_be_able_to_list_active_groups_after_one_shuts_down()
        {
            var probe = CreateTestProbe();
            var managerActor = Sys.ActorOf(DeviceManagerActor.Props());
            
            managerActor.Tell(new RequestTrackDevice("group1", "device1"), probe.Ref);
            probe.ExpectMsg<DeviceRegistered>();
            var toShutDown = probe.LastSender;

            managerActor.Tell(new RequestTrackDevice("group2", "device2"), probe.Ref);
            probe.ExpectMsg<DeviceRegistered>();

            probe.Watch(toShutDown);
            toShutDown.Tell(PoisonPill.Instance);
            probe.ExpectTerminated(toShutDown);
            
            // using awaitAssert to retry because it might take longer for the groupActor
            // to see the Terminated, that order is undefined
            probe.AwaitAssert(() =>
            {
                managerActor.Tell(new RequestDeviceGroupList(1), probe.Ref);
                probe.ExpectMsg<ReplyDeviceGroupList>(x => x.RequestId == 1 && 
                                                      !x.Ids.Contains("group1") && 
                                                      x.Ids.Contains("group2"));
            });
        }
*/
    }
}