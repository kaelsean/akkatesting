using System;
using Akka.TestKit.Xunit2;
using AkkaTesting.DeviceApp.Actors;
using AkkaTesting.DeviceApp.Messages.Device;
using AkkaTesting.DeviceApp.Messages.Temperature;
using FluentAssertions;
using Xunit;

namespace AkkaTesting.Test
{
    public class DeviceActorTests : TestKit
    {
        [Fact]
        public void Device_actor_must_reply_with_latest_temperature_reading()
        {
            var probe = CreateTestProbe();
            var deviceActor = Sys.ActorOf(DeviceActor.Props("group", "device"));

            deviceActor.Tell(new RecordTemperature(requestId: 1, value: 24.0), probe.Ref);
            probe.ExpectMsg<TemperatureRecorded>(s => s.RequestId == 1);

            deviceActor.Tell(new ReadTemperature(requestId: 2), probe.Ref);
            var response = probe.ExpectMsg<RespondTemperature>();
            
            response.RequestId.Should().Be(2);
            response.Value.Should().Be(24.0);
        }
        
        [Fact]
        public void Device_actor_must_reply_with_empty_reading_if_no_temperature_is_known()
        {
            var probe = CreateTestProbe();
            var deviceActor = Sys.ActorOf(DeviceActor.Props("group", "device"));

            deviceActor.Tell(new ReadTemperature(requestId: 42), probe.Ref);
            var response = probe.ExpectMsg<RespondTemperature>();
            
            response.RequestId.Should().Be(42);
            response.Value.Should().Be(null);
        }

        [Fact]
        public void Device_actor_must_reply_to_registration_requests()
        {
            var probe = CreateTestProbe();
            var deviceActor = Sys.ActorOf(DeviceActor.Props("group", "device"));

            deviceActor.Tell(new RequestTrackDevice("group", "device"), probe.Ref);
            probe.ExpectMsg<DeviceRegistered>();

            probe.LastSender.Should().Be(deviceActor);
        }
        
        [Fact]
        public void Device_actor_must_ignore_wrong_registration_requests()
        {
            var probe = CreateTestProbe();
            var deviceActor = Sys.ActorOf(DeviceActor.Props("group", "device"));
            
            deviceActor.Tell(new RequestTrackDevice("invalidGroup", "device"), probe.Ref);
            probe.ExpectNoMsg(TimeSpan.FromMilliseconds(500));
            
            deviceActor.Tell(new RequestTrackDevice("group", "invalidDevice"), probe.Ref);
            probe.ExpectNoMsg(TimeSpan.FromMilliseconds(500));
        }
    }
}