﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
// ReSharper disable once LocalFunctionCanBeMadeStatic
// ReSharper disable once ConvertToLocalFunction
// ReSharper disable StringLiteralTypo

namespace AkkaTesting.Expressions
{
    [ExcludeFromCodeCoverage]
    public static class SomeExpressions
    {
        public static void Expss()
        {
            
            static bool FuncStatic(int i) => i > 7;
            Console.WriteLine($"FuncStatic: {FuncStatic(2)}");
            
            bool Func(int i) => i > 7;
            Console.WriteLine($"Func: {Func(2)}");

            Func<int, bool> func = i => i > 6;
            Console.WriteLine($"func: {func(9)}");

            Expression<Func<int, bool>> exp = i => i > 5;

            Console.WriteLine($"body: {exp.Body}");
            Console.WriteLine($"nodetype: {exp.Body.NodeType}");
            Console.WriteLine($"name: {exp.Name}");
            Console.WriteLine($"returntype: {exp.ReturnType}");
            Console.WriteLine($"canreduce: {exp.CanReduce}");
            Console.WriteLine($"tailcall: {exp.TailCall}");
            foreach (var parameterExpression in exp.Parameters)
            {
                Console.WriteLine($"param: {parameterExpression.Name} {parameterExpression.NodeType} {parameterExpression.IsByRef}");
            }

            Console.WriteLine($"result: {exp.Compile()(5)}");

            var pe = Expression.Parameter(typeof(int), "i");
            var ce = Expression.Constant(10, typeof(int));
            var body = Expression.GreaterThan(pe, ce);
            var et = Expression.Lambda<Func<int, bool>>(body, pe);

            Console.WriteLine($"et result: {et.Compile()(15)}");
        }
    }
}