﻿namespace AkkaTesting.DeviceApp.States.Temperature
{
    public sealed class DeviceNotAvailable : ITemperatureReading
    {
        public static DeviceNotAvailable Instance { get; } = new();

        private DeviceNotAvailable() { }
    }
}