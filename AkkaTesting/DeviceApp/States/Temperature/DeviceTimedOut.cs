﻿namespace AkkaTesting.DeviceApp.States.Temperature
{
    public class DeviceTimedOut : ITemperatureReading
    {
        public static DeviceTimedOut Instance { get; } = new();

        private DeviceTimedOut() { }
    }
}