﻿namespace AkkaTesting.DeviceApp.States.Temperature
{
    public sealed class TemperatureNotAvailable : ITemperatureReading
    {
        public static TemperatureNotAvailable Instance { get; } = new();

        private TemperatureNotAvailable() { }
    }
}