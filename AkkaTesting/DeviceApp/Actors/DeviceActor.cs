﻿using Akka.Actor;
using Akka.Event;
using AkkaTesting.DeviceApp.Messages.Device;
using AkkaTesting.DeviceApp.Messages.Temperature;

namespace AkkaTesting.DeviceApp.Actors
{
    public class DeviceActor : UntypedActor
    {
        private double? _lastTemperatureReading;

        // ReSharper disable once MemberCanBePrivate.Global
        public DeviceActor(string groupId, string deviceId)
        {
            GroupId = groupId;
            DeviceId = deviceId;
        }

        private ILoggingAdapter Logger { get; } = Context.GetLogger();
        private string GroupId { get; }
        private string DeviceId { get; }

        protected override void PreStart() => Logger.Info($"Device actor {GroupId}-{DeviceId} started");
        protected override void PostStop() => Logger.Info($"Device actor {GroupId}-{DeviceId} stopped");
        
        protected override void OnReceive(object message)
        {
            switch (message)
            {
                case RequestTrackDevice track when track.GroupId.Equals(GroupId) && track.DeviceId.Equals(DeviceId):
                    Sender.Tell(DeviceRegistered.Instance);
                    break;
                case RequestTrackDevice track:
                    Logger.Warning($"Ignoring TrackDevice request for {track.GroupId}-{track.DeviceId}.This actor is responsible for {GroupId}-{DeviceId}.");
                    break;
                case RecordTemperature record:
                    Logger.Info($"Recorded temperature reading {record.Value} with {record.RequestId}");
                    _lastTemperatureReading = record.Value;
                    Sender.Tell(new TemperatureRecorded(record.RequestId));
                    break;
                case ReadTemperature read:
                    Sender.Tell(new RespondTemperature(read.RequestId, _lastTemperatureReading));
                    break;
            }
        }
        
        public static Props Props(string groupId, string deviceId) =>
            Akka.Actor.Props.Create(() => new DeviceActor(groupId, deviceId));
    }
}