﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Akka.Actor;
using Akka.Event;
using AkkaTesting.DeviceApp.Messages.DeviceGroup;
using AkkaTesting.DeviceApp.Messages.Temperature;
using AkkaTesting.DeviceApp.States.Temperature;

namespace AkkaTesting.DeviceApp.Actors.Queries
{
    public partial class DeviceGroupQuery : UntypedActor
    {
        private readonly ICancelable _queryTimeoutTimer;

        // ReSharper disable once MemberCanBePrivate.Global
        public DeviceGroupQuery(Dictionary<IActorRef, string> deviceActors, long requestId, IActorRef requester, TimeSpan timeout)
        {
            DeviceActors = deviceActors;
            RequestId = requestId;
            Requester = requester;
            Timeout = timeout;

            _queryTimeoutTimer = Context.System.Scheduler.ScheduleTellOnceCancelable(Timeout, Self, CollectionTimeout.Instance, Self);

            Become(WaitingForReplies(new Dictionary<string, ITemperatureReading>(), new HashSet<IActorRef>(deviceActors.Keys)));
        }

        private ILoggingAdapter Logger { get; } = Context.GetLogger();

        private Dictionary<IActorRef, string> DeviceActors { get; }
        private long RequestId { get; }
        private IActorRef Requester { get; }
        private TimeSpan Timeout { get; }

        protected override void PreStart()
        {
            Logger.Info($"Device group query for {{requester:{Requester.Path.Name}}} started:");
            foreach (var deviceActor in DeviceActors.Keys)
            {
                Context.Watch(deviceActor);
                deviceActor.Tell(new ReadTemperature(0));
            }
        }

        protected override void PostStop()
        {
            Logger.Info($"Device group query for {{requester:{Requester.Path.Name}}} stopped:");
            _queryTimeoutTimer.Cancel();
        }

        [ExcludeFromCodeCoverage]
        protected override void OnReceive(object message) { }

        public static Props Props(Dictionary<IActorRef, string> deviceActors, long requestId, IActorRef requester, TimeSpan timeout) =>
            Akka.Actor.Props.Create(() => new DeviceGroupQuery(deviceActors, requestId, requester, timeout));
    }
}