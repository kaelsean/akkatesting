﻿using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using AkkaTesting.DeviceApp.Messages.DeviceGroup;
using AkkaTesting.DeviceApp.Messages.Temperature;
using AkkaTesting.DeviceApp.States.Temperature;

namespace AkkaTesting.DeviceApp.Actors.Queries
{
    public partial class DeviceGroupQuery
    {
        private UntypedReceive WaitingForReplies(Dictionary<string, ITemperatureReading> repliesSoFar, HashSet<IActorRef> stillWaiting) => message =>
        {
            switch (message)
            {
                case RespondTemperature {RequestId: 0} response:
                    var deviceActor = Sender;

                    ITemperatureReading reading;
                    if (response.Value.HasValue) reading = new Temperature(response.Value.Value);
                    else reading = TemperatureNotAvailable.Instance;

                    ReceivedResponse(deviceActor, reading, stillWaiting, repliesSoFar);
                    break;
                case Terminated terminated:
                    ReceivedResponse(terminated.ActorRef, DeviceNotAvailable.Instance, stillWaiting, repliesSoFar);
                    break;
                case CollectionTimeout:
                    var replies = new Dictionary<string, ITemperatureReading>(repliesSoFar);

                    foreach (var deviceId in stillWaiting.Select(actorRef => DeviceActors[actorRef]))
                        replies.Add(deviceId, DeviceTimedOut.Instance);

                    Requester.Tell(new RespondAllTemperatures(RequestId, replies));
                    Context.Stop(Self);
                    break;
            }
        };

        private void ReceivedResponse(
            IActorRef deviceActor,
            ITemperatureReading reading,
            HashSet<IActorRef> stillWaiting,
            Dictionary<string, ITemperatureReading> repliesSoFar)
        {
            Context.Unwatch(deviceActor);
            var deviceId = DeviceActors[deviceActor];
            stillWaiting.Remove(deviceActor);

            repliesSoFar.Add(deviceId, reading);

            if (stillWaiting.Count == 0)
            {
                Requester.Tell(new RespondAllTemperatures(RequestId, repliesSoFar));
                Context.Stop(Self);
            }
            else Context.Become(WaitingForReplies(repliesSoFar, stillWaiting));
        }
    }
}