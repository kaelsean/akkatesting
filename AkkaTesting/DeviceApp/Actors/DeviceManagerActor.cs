﻿using System.Collections.Generic;
using Akka.Actor;
using Akka.Event;
using AkkaTesting.DeviceApp.Messages.Device;
using AkkaTesting.DeviceApp.Messages.DeviceGroup;

namespace AkkaTesting.DeviceApp.Actors
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class DeviceManagerActor : UntypedActor
    {
        private readonly Dictionary<string, IActorRef> _actors = new();
        private readonly Dictionary<IActorRef, string> _groupIds = new();

        private ILoggingAdapter Logger { get; } = Context.GetLogger();

        protected override void PreStart() => Logger.Info("DeviceManager started");
        protected override void PostStop() => Logger.Info("DeviceManager stopped");

        protected override void OnReceive(object message)
        {
            switch (message)
            {
                case RequestTrackDevice track:
                    if (_actors.TryGetValue(track.GroupId, out var groupRef))
                    {
                        groupRef.Forward(track);
                    }
                    else
                    {
                        Logger.Info($"Creating device group actor for {track.GroupId}");
                        var groupActor = Context.ActorOf(DeviceGroupActor.Props(track.GroupId), $"group-{track.GroupId}");

                        Context.Watch(groupActor);
                        _actors.Add(track.GroupId, groupActor);
                        _groupIds.Add(groupActor, track.GroupId);

                        groupActor.Forward(track);
                    }
                    break;
                case RequestDeviceGroupList deviceGroupList:
                    Sender.Tell(new ReplyDeviceGroupList(deviceGroupList.RequestId, new HashSet<string>(_actors.Keys)));
                    break;
                case Terminated terminated:
                    var groupId = _groupIds[terminated.ActorRef];
                    Logger.Info($"Device group actor for {groupId} has been terminated");

                    _actors.Remove(groupId);
                    _groupIds.Remove(terminated.ActorRef);
                    
                    break;
            }
        }
        
        public static Props Props() => Akka.Actor.Props.Create<DeviceManagerActor>();
    }
}