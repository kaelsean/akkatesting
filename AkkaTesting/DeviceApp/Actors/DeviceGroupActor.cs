﻿using System;
using System.Collections.Generic;
using Akka.Actor;
using Akka.Event;
using AkkaTesting.DeviceApp.Actors.Queries;
using AkkaTesting.DeviceApp.Messages.Device;
using AkkaTesting.DeviceApp.Messages.Temperature;

namespace AkkaTesting.DeviceApp.Actors
{
    public class DeviceGroupActor : UntypedActor
    {
        private readonly Dictionary<string, IActorRef> _actors = new();
        private readonly Dictionary<IActorRef, string> _deviceIds = new();

        // ReSharper disable once MemberCanBePrivate.Global
        public DeviceGroupActor(string groupId) => GroupId = groupId;

        private string GroupId { get; }
        private ILoggingAdapter Logger { get; } = Context.GetLogger();
        
        protected override void PreStart() => Logger.Info($"Device group {GroupId} started");
        protected override void PostStop() => Logger.Info($"Device group {GroupId} stopped");

        protected override void OnReceive(object message)
        {
            switch (message)
            {
                case RequestAllTemperatures request:
                    Context.ActorOf(DeviceGroupQuery.Props(_deviceIds, request.RequestId, Sender, TimeSpan.FromSeconds(3)));
                    break;
                case RequestTrackDevice track when track.GroupId.Equals(GroupId):
                    if (_actors.TryGetValue(track.DeviceId, out var actorRef))
                    {
                        actorRef.Forward(track);
                    }
                    else
                    {
                        Logger.Info($"Creating device actor for {track.DeviceId}");
                        var deviceActor = Context.ActorOf(DeviceActor.Props(track.GroupId, track.DeviceId), $"device-{track.DeviceId}");

                        Context.Watch(deviceActor);
                        _actors.Add(track.DeviceId, deviceActor);
                        _deviceIds.Add(deviceActor, track.DeviceId);
                        
                        deviceActor.Forward(track);
                    }
                    break;
                case RequestTrackDevice track:
                    Logger.Warning($"Ignoring TrackDevice request for {track.GroupId}. This actor is responsible for {GroupId}.");
                    break;
                case RequestDeviceList deviceList:
                    Sender.Tell(new ReplyDeviceList(deviceList.RequestId, new HashSet<string>(_actors.Keys)));
                    break;
                case Terminated terminated:
                    var deviceId = _deviceIds[terminated.ActorRef];
                    Logger.Info($"Device actor for {deviceId} has been terminated");
                    
                    _actors.Remove(deviceId);
                    _deviceIds.Remove(terminated.ActorRef);
                    
                    break;
            }
        }

        public static Props Props(string groupId) => Akka.Actor.Props.Create(() => new DeviceGroupActor(groupId));
    }
}