﻿namespace AkkaTesting.DeviceApp.Messages.Device
{
    public sealed class DeviceRegistered
    {
        public static DeviceRegistered Instance { get; } = new DeviceRegistered();

        private DeviceRegistered() { }
    }
}