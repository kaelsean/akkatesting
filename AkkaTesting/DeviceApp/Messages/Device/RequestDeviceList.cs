﻿namespace AkkaTesting.DeviceApp.Messages.Device
{
    public sealed class RequestDeviceList
    {
        public RequestDeviceList(long requestId) => RequestId = requestId;

        public long RequestId { get; }
    }
}