﻿namespace AkkaTesting.DeviceApp.Messages.DeviceGroup
{
    public sealed class CollectionTimeout
    {
        public static CollectionTimeout Instance { get; } = new();

        private CollectionTimeout() { }
    }
}