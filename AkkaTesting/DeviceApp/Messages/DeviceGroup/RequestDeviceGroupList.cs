﻿namespace AkkaTesting.DeviceApp.Messages.DeviceGroup
{
    public sealed class RequestDeviceGroupList
    {
        public RequestDeviceGroupList(long requestId) => RequestId = requestId;

        public long RequestId { get; }
    }
}