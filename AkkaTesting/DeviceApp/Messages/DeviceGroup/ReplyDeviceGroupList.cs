﻿using System.Collections.Generic;

namespace AkkaTesting.DeviceApp.Messages.DeviceGroup
{
    public class ReplyDeviceGroupList
    {
        public ReplyDeviceGroupList(long requestId, ISet<string> ids)
        {
            RequestId = requestId;
            Ids = ids;
        }

        public long RequestId { get; }
        public ISet<string> Ids { get; }
    }
}