﻿using System.Collections.Generic;
using AkkaTesting.DeviceApp.States.Temperature;

namespace AkkaTesting.DeviceApp.Messages.Temperature
{
    public sealed class RespondAllTemperatures
    {
        public RespondAllTemperatures(long requestId, Dictionary<string, ITemperatureReading> temperatures)
        {
            RequestId = requestId;
            Temperatures = temperatures;
        }

        public long RequestId { get; }
        public Dictionary<string, ITemperatureReading> Temperatures { get; }
    }
}