﻿namespace AkkaTesting.DeviceApp.Messages.Temperature
{
    public sealed class ReadTemperature
    {
        public ReadTemperature(long requestId) => RequestId = requestId;
        
        public long RequestId { get; }
    }
}