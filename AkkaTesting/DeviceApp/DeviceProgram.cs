﻿using System;
using Akka.Actor;
using Akka.Configuration;
using System.Diagnostics.CodeAnalysis;
using AkkaTesting.DeviceApp.Actors;

namespace AkkaTesting.DeviceApp
{
    [ExcludeFromCodeCoverage]
    public static class DeviceProgram
    {
        public static void Run()
        {
            // var fluentConfig = 

            using var system = ActorSystem.Create("actor-server", Config.Empty);
            system.ActorOf<DeviceManagerActor>("manager-actor");

            Console.ReadKey();
        }
    }
}