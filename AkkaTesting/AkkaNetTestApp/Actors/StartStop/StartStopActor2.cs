﻿using System.Diagnostics.CodeAnalysis;
using Akka.Actor;
using Akka.Event;

namespace AkkaTesting.AkkaNetTestApp.Actors.StartStop
{    
    // ReSharper disable once ClassNeverInstantiated.Global
    [ExcludeFromCodeCoverage]
    internal class StartStopActor2 : UntypedActor
    {
        private readonly ILoggingAdapter _logger = Context.GetLogger();
        
        protected override void PreStart() => _logger.Info("second started");
        protected override void PostStop() => _logger.Info("second stopped");

        protected override void OnReceive(object message) { }
    }
}