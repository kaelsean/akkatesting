﻿using System.Diagnostics.CodeAnalysis;
using Akka.Actor;
using Akka.Event;

namespace AkkaTesting.AkkaNetTestApp.Actors.StartStop
{
    // ReSharper disable once ClassNeverInstantiated.Global
    [ExcludeFromCodeCoverage]
    internal class StartStopActor1 : UntypedActor
    {
        private readonly ILoggingAdapter _logger = Context.GetLogger();
        
        protected override void PreStart()
        {
            _logger.Info("first started");
            Context.ActorOf(Props.Create<StartStopActor2>(), "second");
        }

        protected override void PostStop() => _logger.Info("first stopped");

        protected override void OnReceive(object message)
        {
            switch (message)
            {
                case "stop":
                    Context.Stop(Self);
                    break;
            }
        }
    }
    
}