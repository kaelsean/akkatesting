﻿using System.Diagnostics.CodeAnalysis;
using Akka.Actor;
using Akka.Event;

namespace AkkaTesting.AkkaNetTestApp.Actors.Iot
{
    // ReSharper disable once ClassNeverInstantiated.Global
    [ExcludeFromCodeCoverage]
    public class IotSupervisorActor : UntypedActor
    {
        private readonly ILoggingAdapter _logger = Context.GetLogger();
        
        protected override void PreStart() => _logger.Info("IoT Application started");
        protected override void PostStop() => _logger.Info("IoT Application stopped");

        // No need to handle any messages
        protected override void OnReceive(object message) { }

        public static Props Props() => Akka.Actor.Props.Create<IotSupervisorActor>();
    }
}