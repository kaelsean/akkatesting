﻿using System;
using System.Diagnostics.CodeAnalysis;
using Akka.Actor;

namespace AkkaTesting.AkkaNetTestApp.Actors.Print
{
    // ReSharper disable once ClassNeverInstantiated.Global
    [ExcludeFromCodeCoverage]
    internal class PrintMyActorRefActor : UntypedActor
    {
        protected override void OnReceive(object message)
        {
            switch (message)
            {
                case "printiit":
                    var secondRef = Context.ActorOf(Props.Empty, "second-actor");
                    Console.WriteLine($"Second: {secondRef}");
                    Console.WriteLine(secondRef.Path);
                    break;
            }
        }
    }
}