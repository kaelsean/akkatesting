﻿using System.Diagnostics.CodeAnalysis;
using Akka.Actor;

namespace AkkaTesting.AkkaNetTestApp.Actors.Supervisation
{
    // ReSharper disable once ClassNeverInstantiated.Global
    [ExcludeFromCodeCoverage]
    public class SupervisingActor : UntypedActor
    {
        private readonly IActorRef _child = Context.ActorOf(Props.Create<SupervisedActor>(), "supervised-actor");

        protected override void OnReceive(object message)
        {
            switch (message)
            {
                case "failChild":
                    _child.Tell("fail");
                    break;
            }
        }
    }
}