﻿using System;
using System.Diagnostics.CodeAnalysis;
using Akka.Actor;
using Akka.Event;

namespace AkkaTesting.AkkaNetTestApp.Actors.Supervisation
{
    // ReSharper disable once ClassNeverInstantiated.Global
    [ExcludeFromCodeCoverage]
    public class SupervisedActor : UntypedActor
    {
        private readonly ILoggingAdapter _logger = Context.GetLogger();
        
        protected override void PreStart() => _logger.Info("supervised actor started");
        protected override void PostStop() => _logger.Info("supervised actor stopped");

        protected override void OnReceive(object message)
        {
            switch (message)
            {
                case "fail":
                    _logger.Info("supervised actor fails now");
                    throw new Exception("I failed!");
            }
        }
    }
}