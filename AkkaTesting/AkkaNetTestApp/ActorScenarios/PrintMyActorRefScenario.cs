﻿using System;
using System.Diagnostics.CodeAnalysis;
using Akka.Actor;
using AkkaTesting.AkkaNetTestApp.Actors.Print;

namespace AkkaTesting.AkkaNetTestApp.ActorScenarios
{
    [ExcludeFromCodeCoverage]
    public class PrintMyActorRefScenario : BaseScenario
    {
        public override void Play()
        {
            var system = ActorSystem.Create("actor-system");
            
            var firstRef = system.ActorOf(Props.Create<PrintMyActorRefActor>(), "first-actor");
            
            Console.WriteLine($"First: {firstRef}");
            Console.WriteLine(firstRef.Path);
            Console.WriteLine();
            
            firstRef.Tell("printiit", ActorRefs.NoSender);
        }
    }
}