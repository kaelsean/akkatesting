﻿using System.Diagnostics.CodeAnalysis;
using Akka.Actor;
using AkkaTesting.AkkaNetTestApp.Actors.Iot;

namespace AkkaTesting.AkkaNetTestApp.ActorScenarios
{
    [ExcludeFromCodeCoverage]
    public class IotScenario : BaseScenario
    {
        public override void Play()
        {
            using var system = ActorSystem.Create("iot-system");
            system.ActorOf(IotSupervisorActor.Props(), "iot-supervisor");
        }
    }
}