﻿using System.Diagnostics.CodeAnalysis;
using Akka.Actor;
using AkkaTesting.AkkaNetTestApp.Actors.Supervisation;

namespace AkkaTesting.AkkaNetTestApp.ActorScenarios
{
    [ExcludeFromCodeCoverage]
    public class SupervisationScenario : BaseScenario
    {
        public override void Play()
        {
            var system = ActorSystem.Create("actor-system");
            
            var supervisingActor = system.ActorOf(Props.Create<SupervisingActor>(), "supervising-actor");
            supervisingActor.Tell("failChild");
        }
    }
}