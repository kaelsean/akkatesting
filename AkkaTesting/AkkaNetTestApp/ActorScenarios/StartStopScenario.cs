﻿using System.Diagnostics.CodeAnalysis;
using Akka.Actor;
using AkkaTesting.AkkaNetTestApp.Actors.StartStop;

namespace AkkaTesting.AkkaNetTestApp.ActorScenarios
{
    [ExcludeFromCodeCoverage]
    public class StartStopScenario : BaseScenario
    {
        public override void Play()
        {
            using var system = ActorSystem.Create("actor-system");

            var first = system.ActorOf(Props.Create<StartStopActor1>(), "first");
            first.Tell("stop");
        }
    }
}