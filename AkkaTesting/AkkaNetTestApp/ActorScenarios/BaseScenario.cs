﻿using System.Diagnostics.CodeAnalysis;

namespace AkkaTesting.AkkaNetTestApp.ActorScenarios
{
    [ExcludeFromCodeCoverage]
    public abstract class BaseScenario
    {
        public abstract void Play();
    }
}