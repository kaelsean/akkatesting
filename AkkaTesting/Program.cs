﻿using System.Diagnostics.CodeAnalysis;
using AkkaTesting.SuccinctlyApp;

namespace AkkaTesting
{
    [ExcludeFromCodeCoverage]
    internal static class Program
    {
        public static void Main()
        {
            SuccinctlyProgram.Run();
        }
    }
}