﻿using Akka.Actor;
using Akka.Event;
using AkkaTesting.SuccinctlyApp.Exceptions;
using AkkaTesting.SuccinctlyApp.Models.Messages;

namespace AkkaTesting.SuccinctlyApp.Actors
{
    public class MusicPlayerActor : ReceiveActor
    {
        private PlaySongMessage _currentSong;
        private readonly ILoggingAdapter _logger = Context.GetLogger();

        public MusicPlayerActor() => StoppedBehavior();

        private void StoppedBehavior()
        {
            Receive<PlaySongMessage>(PlaySong);
            Receive<StopPlayingMessage>(m => _logger.Error($"{m.User}'s player: Can't stop, the player is already stopped."));
        }

        private void PlayingBehavior()
        {
            Receive<PlaySongMessage>(_ => _logger.Error($"{_currentSong.User}'s player: Can't play. Currently playing {_currentSong.Song}."));
            Receive<StopPlayingMessage>(_ => StopPlaying());
        }

        private void PlaySong(PlaySongMessage message)
        {
            _currentSong = message;

            if (message.Song == "Bohemian Rhapsody") throw new SongNotAvailableException("Bohemian Rhapsody is not available");
            if (message.Song == "Stairway to Heaven") throw new MusicSystemCorruptedException("Song in a corrupt state");
            
            _logger.Info($"{_currentSong.User}'s player: Currently playing '{_currentSong.Song}'.");

            var statsActor = Context.ActorSelection("../../statistics");
            statsActor.Tell(message);
            
            Become(PlayingBehavior);
        }
        
        private void StopPlaying()
        {
            _logger.Info($"{_currentSong.User}'s player: Player is currently stopped.");
            _currentSong = null;
            
            Become(StoppedBehavior);
        }
    }
}