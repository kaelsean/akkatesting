﻿using System.Collections.Generic;
using Akka.Actor;
using AkkaTesting.SuccinctlyApp.Exceptions;
using AkkaTesting.SuccinctlyApp.Models.Messages;

namespace AkkaTesting.SuccinctlyApp.Actors
{
    public class MusicPlayerCoordinatorActor : ReceiveActor
    {
        private readonly Dictionary<string, IActorRef> _musicPlayerActors = new();

        public MusicPlayerCoordinatorActor()
        {
            Receive<PlaySongMessage>(PlaySong);
            Receive<StopPlayingMessage>(StopPlaying);
        }

        private void StopPlaying(StopPlayingMessage message)
        {
            var musicPlayerActor = GetMusicPlayerActor(message.User);
            musicPlayerActor?.Tell(message);
        }

        private void PlaySong(PlaySongMessage message)
        {
            var musicPlayerActor = EnsureMusicPlayerActorExists(message.User);
            musicPlayerActor.Tell(message);
        }

        private IActorRef EnsureMusicPlayerActorExists(string user)
        {
            var musicPlayerActorRef = GetMusicPlayerActor(user);
            if (musicPlayerActorRef is not null) return musicPlayerActorRef;

            musicPlayerActorRef = Context.ActorOf<MusicPlayerActor>(user);
            _musicPlayerActors.Add(user, musicPlayerActorRef);

            return musicPlayerActorRef;
        }

        private IActorRef GetMusicPlayerActor(string user)
        {
            _musicPlayerActors.TryGetValue(user, out var musicPlayerActorRef);
            return musicPlayerActorRef;
        }

        protected override SupervisorStrategy SupervisorStrategy() => new OneForOneStrategy(e => e switch
        {
            SongNotAvailableException => Directive.Resume,
            MusicSystemCorruptedException => Directive.Restart,
            _ => Directive.Stop
        });
    }
}