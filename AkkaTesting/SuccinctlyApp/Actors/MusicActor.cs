﻿using Akka.Actor;
using Akka.Event;
using AkkaTesting.SuccinctlyApp.Services.Interfaces;

namespace AkkaTesting.SuccinctlyApp.Actors
{
    public class MusicActor : ReceiveActor
    {
        private readonly IMusicSongService _musicSongService;
        private readonly ILoggingAdapter _logger;
        
        public MusicActor(IMusicSongService musicSongService)
        {
            _musicSongService = musicSongService;
            _logger = Context.GetLogger();
            
            Receive<string>(HandleSongRetrieval);
        }

        private void HandleSongRetrieval(string songName)
        {
            var song = _musicSongService.GetSongByName(songName);
            _logger.Info(song.SongName);
            // do something
        }
    }
}