﻿using System.Collections.Generic;
using Akka.Actor;
using Akka.Event;
using AkkaTesting.SuccinctlyApp.Models.Messages;

namespace AkkaTesting.SuccinctlyApp.Actors
{
    public class SongPerformanceActor : ReceiveActor
    {
        private readonly Dictionary<string, int> _songPerformanceCounter = new();
        private readonly ILoggingAdapter _logger = Context.GetLogger();

        public SongPerformanceActor() => Receive<PlaySongMessage>(IncreaseSongCounter);

        private void IncreaseSongCounter(PlaySongMessage message)
        {
            var counter = 1;
            if (_songPerformanceCounter.ContainsKey(message.Song)) 
                counter = ++_songPerformanceCounter[message.Song];
            else _songPerformanceCounter.Add(message.Song, counter);
            
            _logger.Info($"Song: {message.Song} has been played {counter} times");
        }
    }
}