﻿using System;
using System.Diagnostics.CodeAnalysis;
using Akka.Actor;
using Akka.Configuration;
using AkkaTesting.SuccinctlyApp.Actors;
using AkkaTesting.SuccinctlyApp.Models.Messages;

namespace AkkaTesting.SuccinctlyApp
{
    [ExcludeFromCodeCoverage]
    public static class SuccinctlyProgram
    {
        public static void Run()
        {
            var config = BootstrapSetup.Create().WithConfig(ConfigurationFactory.ParseString(@"
            akka { 
              actor {
                serializers {
                  hyperion = ""Akka.Serialization.HyperionSerializer, Akka.Serialization.Hyperion""
                    }
                    serialization-bindings {
                        ""System.Object"" = hyperion
                    }
                }
            }"));
            var system = ActorSystem.Create("music-player-actor", config);
            
            var dispatcher = system.ActorOf<MusicPlayerCoordinatorActor>("player-coordinator");
            system.ActorOf<SongPerformanceActor>("statistics");

            dispatcher.Tell(new PlaySongMessage("Bohemian Rhapsody", "Zivi"));
            dispatcher.Tell(new PlaySongMessage("Stairway to Heaven", "Givi"));

            Console.Read();
            system.Terminate();
        }
    }
}