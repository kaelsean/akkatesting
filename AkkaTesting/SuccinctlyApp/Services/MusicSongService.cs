﻿using AkkaTesting.SuccinctlyApp.Models;
using AkkaTesting.SuccinctlyApp.Services.Interfaces;

namespace AkkaTesting.SuccinctlyApp.Services
{
    public class MusicSongService : IMusicSongService
    {
        public Song GetSongByName(string songName) => new(songName, System.Array.Empty<byte>());
    }
}