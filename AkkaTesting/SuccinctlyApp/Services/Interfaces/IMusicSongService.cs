﻿using AkkaTesting.SuccinctlyApp.Models;

namespace AkkaTesting.SuccinctlyApp.Services.Interfaces
{
    public interface IMusicSongService
    {
        Song GetSongByName(string songName);
    }
}