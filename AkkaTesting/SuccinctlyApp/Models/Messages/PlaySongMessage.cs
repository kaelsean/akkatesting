﻿namespace AkkaTesting.SuccinctlyApp.Models.Messages
{
    // ReSharper disable once UnusedType.Global
    public sealed class PlaySongMessage
    {
        public PlaySongMessage(string song, string user)
        {
            Song = song;
            User = user;
        }

        public string Song { get; }
        public string User { get; }
    }
}