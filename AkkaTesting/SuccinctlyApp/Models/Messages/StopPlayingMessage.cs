﻿namespace AkkaTesting.SuccinctlyApp.Models.Messages
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class StopPlayingMessage
    {
        public StopPlayingMessage(string user) => User = user;

        public string User { get; }
    }
}