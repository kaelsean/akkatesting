﻿using Akka.Actor;
using Akka.DI.AutoFac;
using AkkaTesting.SuccinctlyApp.Actors;
using AkkaTesting.SuccinctlyApp.Services;
using AkkaTesting.SuccinctlyApp.Services.Interfaces;
using Autofac;

// ReSharper disable InconsistentNaming

namespace AkkaTesting.SuccinctlyApp.DI
{
    public static class SuccinctlyDI
    {
        public static ActorSystem CreateActorSystemWithDI(string actorSystemName)
        {
            var builder = new ContainerBuilder();
            
            builder.RegisterType<MusicSongService>().As<IMusicSongService>();
            builder.RegisterType<MusicActor>().AsSelf();
            var container = builder.Build();

            var system = ActorSystem.Create("music-system");
            // ReSharper disable once UnusedVariable
            var propsResolver = new AutoFacDependencyResolver(container, system);

            return system;
        }
    }
}