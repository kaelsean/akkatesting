﻿using System;

namespace AkkaTesting.SuccinctlyApp.Exceptions
{
    public class MusicSystemCorruptedException : Exception
    {
        public MusicSystemCorruptedException(string message) : base(message) { }
    }
}