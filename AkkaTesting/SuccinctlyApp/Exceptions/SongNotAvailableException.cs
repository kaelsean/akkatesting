﻿using System;

namespace AkkaTesting.SuccinctlyApp.Exceptions
{
    public class SongNotAvailableException : Exception
    {
        public SongNotAvailableException(string message) : base(message) { }
    }
}